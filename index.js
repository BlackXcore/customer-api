const Joi = require("joi");
const express = require("express");
const { CustomerDatabase } = require("./database");
const app = express();

app.use(express.json());

//GET A LIST OF CUSTOMERS
app.get("/api/customers", async (req, res) => {
  let customerDb = new CustomerDatabase();

  customerDb
    .getCustomers()
    .then((listOfCustomers) => {
      res.send(listOfCustomers);
    })
    .catch((error) => {
      res.status(400).send(error.message);
    });
});

//CREATE A NEW CUSTOMER
app.post("/api/customers", (req, res) => {
  error = validateCustomer(req.body);
  if (error) return res.status(400).send(error.message);

  let customerDb = new CustomerDatabase();
  customerDb
    .createCustomer(req.body)
    .then((customer) => {
      res.send(customer[0]);
    })
    .catch((error) => {
      res.status(400).send(error.message);
    });
});

//UPDATE A CUSTOMER
app.put("/api/customers/:email", (req, res) => {
  error = validateCustomer(req.body, false);
  if (error) return res.status(400).send(error.message);

  let customerEmail = req.params.email;
  let customerDb = new CustomerDatabase();

  customerDb
    .updateCustomerByEmail(customerEmail, req.body)
    .then((updatedCustomer) => {
      res.send(updatedCustomer);
    })
    .catch((error) => {
      res.status(400).send(error.message);
    });
});

//GET A SINGLE CUSTOMER
app.get("/api/customers/:email", (req, res) => {
  let customerEmail = req.params.email;

  let customerDb = new CustomerDatabase();

  customerDb
    .getCustomerByEmail(customerEmail)
    .then((customer) => {
      res.send(customer);
    })
    .catch((error) => {
      res.status(400).send(error.message);
    });
});

//DELETE RECORD USING EMAIL
app.delete("/api/customers/:email", (req, res) => {
  let customerEmail = req.params.email;

  let customerDb = new CustomerDatabase();

  customerDb
    .deleteCustomerByEmail(customerEmail)
    .then((customer) => {
      res.send(customer[0]);
    })
    .catch((error) => {
      res.status(400).send(error.message);
    });
});

//SCHEMA TO VALIDATE THE CUSTOMER OBJECT
function validateCustomer(customer, valuesRequired = true) {
  const schema = Joi.object({
    FirstName: Joi.string().min(3).required(),
    LastName: Joi.string().min(3).required(),
    Email: Joi.string().min(3).email().required(),
    HomeAddress: Joi.string().min(5).required(),
    PhoneNumber: Joi.string().min(10).required(),
    JobTitle: Joi.string().min(3),
  });

  const optionalSchema = Joi.object({
    FirstName: Joi.string().min(3),
    LastName: Joi.string().min(3),
    Email: Joi.string().min(3).email(),
    HomeAddress: Joi.string().min(5),
    PhoneNumber: Joi.string().min(10),
    JobTitle: Joi.string().min(3),
  });

  const { error } = valuesRequired
    ? schema.validate(customer)
    : optionalSchema.validate(customer);
  return error;
}

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}`));
