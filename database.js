const sql = require("mssql");
const { dbConfig } = require("./config");

class CustomerDatabase {
  constructor() {
    this.dbConnection = new sql.ConnectionPool(dbConfig);
  }

  async runQuery(query) {
    await this.dbConnection.connect();

    try {
      const request = this.dbConnection.request();
      return await request.query(query);
    } catch (error) {
      throw error;
    }
  }

  getCustomers() {
    return new Promise(async (resolve, reject) => {
      try {
        const query = "SELECT * FROM Customers ORDER BY FirstName";
        const result = await this.runQuery(query);

        resolve(result.recordset);
      } catch (error) {
        reject(Error(`SQL Error: Failed to connect to the serve.`));
      }
    });
  }

  createCustomer(customer) {
    return new Promise(async (resolve, reject) => {
      let customerJobTitle =
        customer.JobTitle != undefined ? customer.JobTitle : "";

      try {
        const query = `INSERT INTO Customers 
        (FirstName, LastName, Email, HomeAddress, PhoneNumber, JobTitle)
        VALUES ('${customer.FirstName}', '${customer.LastName}', '${customer.Email}', 
          '${customer.HomeAddress}', '${customer.PhoneNumber}', '${customerJobTitle}');
          
          SELECT * FROM Customers
          WHERE Email = '${customer.Email}'
        `;

        const result = await this.runQuery(query);
        resolve(result.recordset);
      } catch (error) {
        reject(Error(`Error: Record already exists`));
      }
    });
  }

  getCustomerByEmail(email) {
    return new Promise(async (resolve, reject) => {
      try {
        const query = `
        SELECT * FROM Customers
        WHERE Email = '${email}';`;

        const result = await this.runQuery(query);
        if (result.rowsAffected[0] < 1) {
          reject(Error(`No records with the email (${email}) were found.`));
        }
        resolve(result.recordset[0]);
      } catch (error) {
        reject(Error(`SQL Error: Failed to connect to the serve.`));
      }
    });
  }

  deleteCustomerByEmail(email) {
    return new Promise(async (resolve, reject) => {
      try {
        const query = `
        SELECT * FROM Customers
        WHERE Email = '${email}';
        DELETE FROM Customers WHERE Email = '${email}'`;

        const result = await this.runQuery(query);
        if (result.rowsAffected[0] < 1) {
          reject(Error(`No records with the email (${email}) were found.`));
        }
        resolve(result.recordset);
      } catch (error) {
        reject(Error(`SQL Error: Failed to connect to the serve.`));
      }
    });
  }

  updateCustomerByEmail(email, newCustomerValues) {
    return new Promise(async (resolve, reject) => {
      try {
        let setQuery = this.createSetQuery(newCustomerValues);
        let query = `
          UPDATE Customers
          ${setQuery}
          WHERE Email = '${email}'
          SELECT * FROM Customers
          WHERE Email = '${email}'
          `;

        const result = await this.runQuery(query);
        resolve(result.recordset[0]);
      } catch (err) {
        reject(Error("Error: Failed to update record."));
      }
    });
  }

  createSetQuery(newCustomerValues) {
    let query = "SET ";
    let customerKeys = Object.keys(newCustomerValues);

    customerKeys.forEach((value) => {
      query += `${value} = '${newCustomerValues[value]}', `;
    });
    query = query.trim().replace(/.$/, "");
    return query;
  }
}

module.exports = { CustomerDatabase };
